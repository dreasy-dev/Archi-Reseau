# Proposition d'une architecture réseau pour l'entreprise "MOI"

## Sommaire
- [Présentation](#présentation)
- [Matériel](#matériel)
    - [Site A:](#site-a)
    - [Site B:](#site-b)
    - [Site C (Nouveau site):](#site-c-nouveau-site)
    - [Site Annexe (Nouveau site):](#site-annexe-nouveau-site)
- [Architecture](#architecture)
- [Configuration](#configuration)
    - [Configuration des routeurs](#configuration-des-routeurs)
    - [Configuration switch distrib (Site A)](#configuration-des-switchs-distrib-site-a)
    - [Configuration des switchs d'accès (Site A)](#configuration-des-switchs-daccès-site-a)
    - [Configuration des switchs distrib (Site B)](#configuration-des-switchs-distrib-site-b)
    - [Configuration des switchs d'accès (Site B)](#configuration-des-switchs-daccès-site-b)
    - [Configuration des switchs distrib (Site C)](#configuration-des-switchs-distrib-site-c)
    - [Configuration des switchs d'accès (Site C)](#configuration-des-switchs-daccès-site-c)
    - [Configuration des switchs d'accès (Site Annexe)](#configuration-des-switchs-daccès-site-annexe)
    - [Configuration du serveur DHCP](#configuration-du-serveur-dhcp)
- [Table d'adressage](#table-dadressage)
    - [VLANWeb (VLAN 66)](#vlanweb-vlan-66)
    - [Serveur (VLAN 50)](#serveur-vlan-50)
    - [Poste de travail (tech) (VLAN 10)](#poste-de-travail-tech-vlan-10)
    - [Poste de travail (commerciaux) (VLAN 15)](#poste-de-travail-commerciaux-vlan-15)
    - [Poste de travail (direction) (VLAN 20)](#poste-de-travail-direction-vlan-20)
    - [Poste de travail/Management (Admin + switch) (VLAN 100)](#poste-de-travailmanagement-admin--switch-vlan-100)
    - [Telephone (VLAN 30)](#telephone-vlan-30)
    - [Imprimante (VLAN 40)](#imprimante-vlan-40)
- [Nos conseils:](#nos-conseils)


## Présentation

Dans ce document vous retrouverez la proposition d'une architecture réseau pour l'entreprise "MOI", ainsi que la configuration des différents éléments du réseau.  
Les demandes du client sont les suivantes :
- 4 sites (A, B, C, Annexe)
    - 3 sites (A, B, C) reliés entre eux par des liens fibre
    - 1 site relié en wan (Annexe vers A)
- Site A: (existant)
    - Routeur accès internet (WAN)
    - Serveur DHCP
    - Serveur Web
- Site B: (existant)
    - 60 postes de travail ( augmentation à 180 postes de travail prévue)
    - 60 téléphones IP (augmentation à 180 téléphones IP prévue)
    - 10 imprimantes 
- Site C: (Futur site)
    - 100 postes de travail
    - 100 téléphones IP
    - 10 imprimantes
- Site Annexe: (Futur site)
    - Routeur accès internet (WAN)
    - 10 postes de travail
    - 10 téléphones IP
    - 2 imprimantes
- Création de Vlan
    - Vlan Web (Vlan 66)
    - Vlan Serveur (Vlan 50)
    - Vlan Poste de travail (tech) (Vlan 10)
    - Vlan Poste de travail (commerciaux) (Vlan 15)
    - Vlan Poste de travail (direction) (Vlan 20)
    - Vlan Poste de travail/Management (Admin + switch) (Vlan 100)
    - Vlan Telephone (Vlan 30)
    - Vlan Imprimante (Vlan 40)
- Ip du réseau : 192.168.0.0/16

Pour réduire les coups nous réutiliserons le matériel déjà présent sur le site A et B.
Néanmoins afin d'avoir une meilleur disponibilité du réseau nous ajouterons du matériel sur le site A et B. 

## Matériel

La liste suivante regroupe tout le maétiel necessaire pour effectuer l'installation du réseau elle prend en compte le matériel déjà présent et le matériel à ajouter.

### Site A:
    - 1 Accès internet (WAN) (Nouveau)
    - 2 routeurs physiques (1 logique) (1 présent + 1 nouveau)
    - 2 switchs distrib physiques (1 logique) (1 présent + 1 nouveau)
    - 2 switchs d'accès physiques (1 logique) (2 nouveaux)

### Site B:
    - 2 switchs distrib physiques (1 logique) (1 présent + 1 nouveau)
    - 12 switchs d'accès physiques (6 logique) (3 présent + 9 nouveau)
    - 30 bornes wifi (30 nouveau)

### Site C (Nouveau site):
    - 2 switchs distrib physiques (1 logique) 
    - 8 switchs d'accès physiques (4 logique)
    - 10 bornes wifi

### Site Annexe (Nouveau site):
    - 1 Accès internet (WAN)
    - 1 routeur physique
    - 2 switch d'accès physique (1 logique)
    - 1 borne wifi

## Architecture

![image](./img/schema.png)
Architecture réseau de l'entreprise "MOI" version physique.

![image](./img/schema_logique.png)
Architecture réseau de l'entreprise "MOI" version logique.

## Configuration 
La configuration n'est pas identique pour tout les switchs mais la logique est la meme. Vous avez un exemple de configuration pour chaque type de switch et sur chaque site.
Protocol spanning-tree : PVST
configuration des vlans
configuration des ports
configuration des ports en mode trunk ou access en fonction des besoins.
### Configuration des routeurs
```
interface Serial0/0
 ip address 10.0.0.1 255.0.0.0
  clock rate 2000000
!
interface GigabitEthernet8/0
 no ip address
 duplex auto
 speed auto
 no shutdown 
!
interface GigabitEthernet8/0.10
 encapsulation dot1Q 10
 ip address 192.168.2.254 255.255.254.0
 ip helper-address 192.168.100.250
!
interface GigabitEthernet8/0.15
 encapsulation dot1Q 15
 ip address 192.168.4.254 255.255.255.192
 ip helper-address 192.168.100.250
!
interface GigabitEthernet8/0.20
 encapsulation dot1Q 20
 ip address 192.168.4.110
 ip helper-address 192.168.100.250
!
interface GigabitEthernet8/0.30
 encapsulation dot1Q 30
 ip address 192.168.6.254 255.255.254.0
 ip helper-address 192.168.100.250
!
interface GigabitEthernet8/0.40
 encapsulation dot1Q 40
 ip address 192.168.8.254 255.255.255.224
 ip helper-address 192.168.100.250
!
interface GigabitEthernet8/0.50
 encapsulation dot1Q 50
 ip address 192.168.100.254 255.255.255.0
!
interface GigabitEthernet8/0.66
 encapsulation dot1Q 66
 ip address 192.168.0.254 255.255.255.248
 ip helper-address 192.168.100.250
!
interface GigabitEthernet8/0.100
 encapsulation dot1Q 100
 no ip address
!
ip route 192.168.2.254 255.255.255.254 10.0.0.2 
ip route 192.168.2.0 255.255.254.0 10.0.0.2 
ip route 192.168.6.0 255.255.254.0 10.0.0.2 
```
### Configuration des switchs distrib (Site A)
```
interface Port-channel1
 switchport mode trunk
!
interface Port-channel2
 switchport mode trunk
!
interface Port-channel3
 switchport mode trunk
!
interface FastEthernet0/1
!
interface FastEthernet0/2
!
interface FastEthernet0/3
!
interface FastEthernet0/4
!
interface FastEthernet0/5
!
interface FastEthernet0/6
!
interface FastEthernet0/7
!
interface FastEthernet0/8
!
interface FastEthernet0/9
!
interface FastEthernet0/10
 switchport mode trunk
 channel-protocol lacp
 channel-group 2 mode active
!
interface FastEthernet0/11
 switchport mode trunk
 channel-protocol lacp
 channel-group 2 mode active
!
interface FastEthernet0/12
!
interface FastEthernet0/13
!
interface FastEthernet0/14
!
interface FastEthernet0/15
!
interface FastEthernet0/16
!
interface FastEthernet0/17
!
interface FastEthernet0/18
!
interface FastEthernet0/19
!
interface FastEthernet0/20
!
interface FastEthernet0/21
 switchport mode trunk
 channel-group 3 mode active
!
interface FastEthernet0/22
 switchport mode trunk
 channel-group 3 mode active
!
interface FastEthernet0/23
 switchport mode trunk
 channel-group 1 mode active
!
interface FastEthernet0/24
 switchport mode trunk
 channel-group 1 mode active
!
interface GigabitEthernet0/1
 switchport trunk allowed vlan 10,15,20,30,40,50,66,100
 switchport mode trunk
```
### Configuration des switchs d'accès (Site A)
```
interface Port-channel1
 switchport mode trunk
!
interface Port-channel2
 switchport mode trunk
!
interface Port-channel3
 switchport mode trunk
!
interface FastEthernet0/10
 switchport mode trunk
 channel-protocol lacp
 channel-group 2 mode active
!
interface FastEthernet0/11
 switchport mode trunk
 channel-protocol lacp
 channel-group 2 mode active
!
interface FastEthernet0/21
 switchport mode trunk
 channel-group 3 mode active
!
interface FastEthernet0/22
 switchport mode trunk
 channel-group 3 mode active
!
interface FastEthernet0/23
 switchport mode trunk
 channel-group 1 mode active
!
interface FastEthernet0/24
 switchport mode trunk
 channel-group 1 mode active
!
interface GigabitEthernet0/1
 switchport trunk allowed vlan 10,15,20,30,40,50,66,100
 switchport mode trunk
```
### Configuration des switchs distrib (Site B)
```
interface Port-channel1
 switchport mode trunk
!
interface Port-channel2
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
!
interface Port-channel3
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
!
interface Port-channel4
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
!
interface Port-channel5
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
!
interface Port-channel6
 switchport trunk allowed vlan 40
 switchport mode trunk
!
interface FastEthernet0/3
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
 channel-protocol lacp
 channel-group 2 mode active
!
interface FastEthernet0/4
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
 channel-protocol lacp
 channel-group 3 mode active
!
interface FastEthernet0/5
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
 channel-protocol lacp
 channel-group 3 mode active
!
interface FastEthernet0/6
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
 channel-protocol lacp
 channel-group 4 mode active
!
interface FastEthernet0/7
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
 channel-protocol lacp
 channel-group 4 mode active
!
interface FastEthernet0/8
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
 channel-protocol lacp
 channel-group 5 mode active
!
interface FastEthernet0/9
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
 channel-protocol lacp
 channel-group 5 mode active
!
interface FastEthernet0/10
 switchport trunk allowed vlan 40
 switchport mode trunk
 channel-protocol lacp
 channel-group 6 mode active
!
interface FastEthernet0/11
 switchport trunk allowed vlan 40
 switchport mode trunk
 channel-protocol lacp
 channel-group 6 mode active
!
interface GigabitEthernet0/1
 switchport trunk allowed vlan 10,15,20,40
 switchport mode trunk
 channel-protocol lacp
!
interface GigabitEthernet0/2
 switchport trunk allowed vlan 10,15,20,40
 switchport mode trunk
 channel-protocol lacp
```
### Configuration des switchs d'accès (Site B)
```
interface Port-channel2
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
!
interface Port-channel3
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
!
interface Port-channel4
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
!
interface Port-channel5
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
!
interface Port-channel6
 switchport trunk allowed vlan 40
 switchport mode trunk
!
interface FastEthernet0/1
 switchport trunk native vlan 10
 switchport trunk allowed vlan 10,30
 switchport mode trunk
 switchport voice vlan 30
!
interface FastEthernet0/3
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
 channel-protocol lacp
 channel-group 2 mode active
!
interface FastEthernet0/4
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
 channel-protocol lacp
 channel-group 3 mode active
!
interface FastEthernet0/5
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
 channel-protocol lacp
 channel-group 3 mode active
!
interface FastEthernet0/6
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
 channel-protocol lacp
 channel-group 4 mode active
!
interface FastEthernet0/7
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
 channel-protocol lacp
 channel-group 4 mode active
!
interface FastEthernet0/8
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
 channel-protocol lacp
 channel-group 5 mode active
!
interface FastEthernet0/9
 switchport trunk allowed vlan 10,15,20,30
 switchport mode trunk
 channel-protocol lacp
 channel-group 5 mode active
!
interface FastEthernet0/10
 switchport trunk allowed vlan 40
 switchport mode trunk
 channel-protocol lacp
 channel-group 6 mode active
!
interface FastEthernet0/11
 switchport trunk allowed vlan 40
 switchport mode trunk
 channel-protocol lacp
 channel-group 6 mode active
!
interface FastEthernet0/23
 switchport mode trunk
 channel-protocol lacp
!
interface FastEthernet0/24
 switchport mode trunk
 channel-protocol lacp
!
interface GigabitEthernet0/1
 switchport trunk allowed vlan 10,20,25,30,40,50,100
 switchport mode trunk
 channel-protocol lacp
!
interface GigabitEthernet0/2
 switchport trunk allowed vlan 10,20,25,30,40,50,100
 switchport mode trunk
 channel-protocol lacp
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan100
 no ip address
 ip helper-address 192.168.0.250
```
### Configuration des switchs distrib (Site C)
```

interface Port-channel1
 switchport mode trunk
!
interface Port-channel2
 switchport mode trunk
!
interface Port-channel3
 switchport mode trunk
!
interface Port-channel4
 switchport mode trunk
!
interface Port-channel5
 switchport mode trunk
!
interface Port-channel6
 switchport mode trunk
!
interface FastEthernet0/2
 switchport mode trunk
 channel-protocol lacp
 channel-group 2 mode active
!
interface FastEthernet0/3
 switchport mode trunk
 channel-protocol lacp
 channel-group 2 mode active
!
interface FastEthernet0/4
 switchport mode trunk
 channel-protocol lacp
 channel-group 3 mode active
!
interface FastEthernet0/5
 switchport mode trunk
 channel-protocol lacp
 channel-group 3 mode active
!
interface FastEthernet0/6
 switchport mode trunk
 channel-protocol lacp
 channel-group 4 mode active
!
interface FastEthernet0/7
 switchport mode trunk
 channel-protocol lacp
 channel-group 4 mode active
!
interface FastEthernet0/10
 switchport mode trunk
 channel-protocol lacp
 channel-group 1 mode active
!
interface FastEthernet0/11
 switchport mode trunk
 channel-protocol lacp
 channel-group 1 mode active
!
interface GigabitEthernet0/1
 switchport mode trunk
 channel-protocol lacp
 channel-group 6 mode active
!
interface GigabitEthernet0/2
 switchport mode trunk
 channel-protocol lacp
 channel-group 6 mode active
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan100
 no ip address
 ip helper-address 192.168.0.250
```
### Configuration des switchs d'accès (Site C)
```
interface Port-channel1
 switchport mode trunk
!
interface Port-channel2
 switchport mode trunk
!
interface Port-channel3
 switchport mode trunk
!
interface Port-channel4
 switchport mode trunk
!
interface Port-channel5
 switchport mode trunk
!
interface Port-channel6
 switchport mode trunk
!
interface FastEthernet0/1
 switchport trunk native vlan 15
 switchport trunk allowed vlan 15,30
 switchport mode trunk
 switchport voice vlan 30
!
interface FastEthernet0/2
 switchport mode trunk
 channel-protocol lacp
 channel-group 2 mode active
!
interface FastEthernet0/3
 switchport mode trunk
 channel-protocol lacp
 channel-group 2 mode active
!
interface FastEthernet0/4
 switchport mode trunk
 channel-protocol lacp
 channel-group 3 mode active
!
interface FastEthernet0/5
 switchport mode trunk
 channel-protocol lacp
 channel-group 3 mode active
!
interface FastEthernet0/6
 switchport mode trunk
 channel-protocol lacp
 channel-group 4 mode active
!
interface FastEthernet0/7
 switchport mode trunk
 channel-protocol lacp
 channel-group 4 mode active
!
interface FastEthernet0/10
 switchport mode trunk
 channel-protocol lacp
 channel-group 1 mode active
!
interface FastEthernet0/11
 switchport mode trunk
 channel-protocol lacp
 channel-group 1 mode active
!
interface FastEthernet0/23
 switchport mode trunk
 channel-protocol lacp
!
interface FastEthernet0/24
 switchport mode trunk
 channel-protocol lacp
!
interface GigabitEthernet0/1
 switchport mode trunk
 channel-protocol lacp
!
interface GigabitEthernet0/2
 switchport mode trunk
 channel-protocol lacp
!
interface Vlan1
 no ip address
 shutdown
!
interface Vlan100
 no ip address
 ip helper-address 192.168.0.250

```
### Configuration des switchs d'accès (Site Annexe)
```
!
interface Port-channel1
 switchport mode trunk
!
interface FastEthernet0/1
 switchport mode trunk
 channel-protocol lacp
 channel-group 1 mode active
!
interface FastEthernet0/2
 switchport mode trunk
 channel-protocol lacp
 channel-group 1 mode active
 !
interface FastEthernet0/9
 switchport access vlan 40
 switchport mode access
!
interface FastEthernet0/10
 switchport trunk native vlan 10
 switchport trunk allowed vlan 10,30
 switchport mode trunk
 switchport voice vlan 30
!
interface FastEthernet0/11
 switchport access vlan 40
 switchport mode access
!
interface GigabitEthernet0/1
 switchport trunk allowed vlan 10,15,20,30,40,50,66,100
 switchport mode trunk
!
interface Vlan1
 no ip address
 shutdown
 !
 interface Vlan100
 no ip address
 ip helper-address 192.168.0.250
```
### Configuration du serveur DHCP
![image](./img/dhcp.png)
Le serveur DHCP est configuré pour distribuer les adresses IP en fonction des VLANs. Selon la table d'adressage ci-dessous.
## Table d'adressage
Les plages d'adresses IP ont été calculées en fonction du nombre d'adresses IP nécessaires en prenant en compte les besoins actuel et futur de l'entreprise.

Première adresse : 192.168.0.1  
Dernière adresse : 192.168.255.254  
Adresse de broadcast : 192.168.255.255  
Nombre d'adresses IP disponibles : 65534  

### Serveur WEB (VLAN 66) 
Masque de réseau : 255.255.255.248  
Adresse de réseau : 192.168.0.0  
Première adresse : 192.168.0.1  
Dernière adresse : 192.168.0.6  
Adresse de broadcast : 192.168.0.7  
Nombre d'adresses IP disponibles : 6
Gateway : 192.168.0.254

### Serveur (VLAN 50) 
Masque de réseau : 255.255.255.252  
Adresse de réseau : 192.168.100.248  
Première adresse : 192.168.100.249  
Dernière adresse : 192.168.100.250  
Adresse de broadcast : 192.168.100.251  
Nombre d'adresses IP disponibles : 2
Gateway : 192.168.100.254
Serveur DHCP : 192.168.100.250

### Poste de travail (tech) (VLAN 10)
Masque de réseau : 255.255.254.0  
Adresse de réseau : 192.168.2.0  
Première adresse : 192.168.2.1  
Dernière adresse : 192.168.3.254  
Adresse de broadcast : 192.168.3.255  
Nombre d'adresses IP disponibles : 510  
Gateway : 192.168.2.254

### Poste de travail (commerciaux) (VLAN 15)
Masque de réseau : 255.255.255.192  
Adresse de réseau : 192.168.4.0  
Première adresse : 192.168.4.1  
Dernière adresse : 192.168.4.62  
Adresse de broadcast : 192.168.4.63  
Nombre d'adresses IP disponibles : 62 
Gateway : 192.168.4.254

### Poste de travail (direction) (VLAN 20)
Masque de réseau : 255.255.255.240  
Adresse de réseau : 192.168.4.96  
Première adresse : 192.168.4.97  
Dernière adresse : 192.168.4.110  
Adresse de broadcast : 192.168.4.111  
Nombre d'adresses IP disponibles : 14
Gateway : 192.168.4.110

### Poste de travail/Management (Admin + switch) (VLAN 100)
 
Masque de réseau : 255.255.255.192  
Adresse de réseau : 192.168.0.128  
Première adresse : 192.168.0.129  
Dernière adresse : 192.168.0.190  
Adresse de broadcast : 192.168.0.191  
Nombre d'adresses IP disponibles : 62  
Gateway : 192.168.0.191

### Telephone (VLAN 30)
Masque de réseau : 255.255.254.0  
Adresse de réseau : 192.168.6.0  
Première adresse : 192.168.6.1  
Dernière adresse : 192.168.7.254  
Adresse de broadcast : 192.168.7.255  
Nombre d'adresses IP disponibles : 510 
Gateway : 192.168.6.254 

### Imprimante (VLAN 40)
Masque de réseau : 255.255.255. 224 
Adresse de réseau : 192.168.8.0  
Première adresse : 192.168.8.1  
Dernière adresse : 192.168.8.30  
Adresse de broadcast : 192.168.8.31  
Nombre d'adresses IP disponibles : 30  
Gateway : 192.168.8.254

## Nos conseils:
Pour une meilleur disponibilité du réseau nous vous conseillons si possible une laison opérateur backup sur le site B avec un routeur firewall, ainsi qu'un serveur WEB et DHCP backup sur le site B. Et de relier le site C au site B par 2 lien fibre. Ainsi en cas de panne sur le site A le site B prendra le relais.